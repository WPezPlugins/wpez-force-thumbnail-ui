<?php
/**
 * Plugin Name: WPezPlugins: Force Thumbnail UI
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-force-thumbnail-ui
 * Description: Admin UI for WPezPlugins: Force Thumbnail (https://gitlab.com/WPezPlugins/wpez-force-thumbnail).
 * Version: 0.0.1
 * Author: Mark F. Simchock (Chief Executive Alchemist @ Alchemy United)
 * Author URI: https://AlchemyUnited.com/utm_source=wpez_ftui
 * Text Domain: wpez_ftui
 * License: GPLv2 or later
 *
 * @package WPezForceThumbnail
 */

namespace WPezForceThumbnail;

defined( 'ABSPATH' ) || exit;

/**
 * A function that centralizes the plugin's settings.
 *
 * @param string $key The setting being requested.
 * @param mixed $fallback If the setting requested is not found, return this value.
 *
 * @return mixed
 */
function uiSettings( string $key, $fallback = '' ) {

	$arr = array(
		'option_group'                      => 'media',
		'option_namespace'                  => 'wpez_force_thumbnail_option',
		'option_checkbox_suffix'            => '_checkbox',
		'option_img_id_suffix'              => '_img_id',
		'display_tax_term_children_default' => false,
		'label_img_id_global'               => __( 'Global Img ID', 'wpez_ftui' ),
		'section_title'                     => __( 'WPezPlugins: Force Thumbnail', 'wpez_ftui' ),
		'section_title_error'               => __( 'Error', 'wpez_ftui' ),
		'section_error_msg'                 => __( 'Error - The parent plugin must be installed and activated.', 'wpez_ftui' ),
		'field_deactivate'                  => 'plugin_deactivated',
		'title_deactivate'                  => __( '&nbsp;', 'wpez_ftui' ),
		'label_checkbox_deactivate'         => __( 'Remove all Force Thumbnail data from the DB when the plugin is deactivated', 'wpez_ftui' ),
		'label_checkbox_one'                => __( 'Enable Force Thumbnail for Post Type: %post_type%', 'wpez_ftui' ),
		'label_checkbox_two'                => __( 'Add the Force Thumbnail image to the database for Post Type: %post_type%', 'wpez_ftui' ),
		'label_checkbox_use_first_img'      => __( 'Use the first image from The Content for the Post Type: %post_type%', 'wpez_ftui' ),
		'label_checkbox_use_tax'            => __( 'Use the Taxonomy (list below) for the Post Type: %post_type%', 'wpez_ftui' ),
		'label_checkbox_use_post'           => __( 'Use the Img ID (in the field below) for the Post Type: %post_type%', 'wpez_ftui' ),
		'label_img_id_prefix'               => __( 'Post Type: ', 'wpez_ftui' ),
		'label_img_id_suffix'               => __( ' > Img ID ', 'wpez_ftui' ),
	);

	$arr_new = apply_filters( __NAMESPACE__ . 'ui_settings' , array(), $arr );
	if ( is_array( $arr_new ) ) {
		$arr = array_merge( $arr, $arr_new );
	}

	if ( isset( $arr[ $key ] ) ) {
		return $arr[ $key ];
	}
	return $fallback;
}


register_activation_hook( __FILE__, __NAMESPACE__ . '\registerActivation' );
/**
 * Callback for register_activation_hook().
 *
 * @return void
 */
function registerActivation() {
	// TODO - add any activation stuff here.
}


register_deactivation_hook( __FILE__, __NAMESPACE__ . '\registerDeactivation' );
/**
 * Callback for register_deactivation_hook(). Deletes the plugin's options if necessary.
 *
 * @return void
 */
function registerDeactivation() {

	global $wpdb;

	$str_option = uiSettings( 'option_namespace' ) . '_checkbox';
	$str_field  = uiSettings( 'field_deactivate' );
	$value = getOption( $str_option, $str_field, '' );

	if ( sanitizeInputCheckboxSingle( $value ) ) {

		$ret = $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}options WHERE option_name LIKE %s", uiSettings( 'option_namespace' ) . '_%' ) );
	}
}

add_action( 'admin_notices', __NAMESPACE__ . '\adminNoticesError' );
/**
 * If the parent plugin is not active - and this plugin is - then add an admin notice.
 *
 * @return void
 */
function adminNoticesError() {

	if ( ! pluginParentActive() ) {
		?>
		<div class="notice notice-error is-dismissible"><p>
			<?php echo 'WPezPlugins: Force Thumbnail UI - ' . wp_kses_post( uiSettings( 'section_error_msg' ) ); ?>
		</p></div>
		<?php
	}
}

/**
 * Checks to see if the parent plugin is active.
 *
 * @return bool
 */
function pluginParentActive() {

	if ( function_exists( __NAMESPACE__ . '\checkThemeSupport' ) ) {
		return true;
	}
	return false;
}


/* Admin init */
add_action( 'admin_init', __NAMESPACE__ . '\adminInit' );
/**
 * Adds Admin UI via the WP Settings API.
 *
 * @return void
 */
function adminInit() {

	$str_title_error  = ' - ' . uiSettings( 'section_title_error' );
	$str_option_group = uiSettings( 'option_group' );

	$str_prefix             = uiSettings( 'option_namespace' );
	// The plugin uses two options (with multiple values stored in each). Each option has it's own sanitation (/validation) callback.
	$str_option_name_chk    = $str_prefix . '_checkbox';
	$str_option_name_img_id = $str_prefix . '_img_id';

	// Register checkbox(s).
	register_setting(
		$str_option_group,
		$str_option_name_chk,
		array(
			'sanitize_callback' => __NAMESPACE__ . '\sanitizeInputCheckbox'
		),
	);

	// Register img id(s).
	register_setting(
		$str_option_group,
		$str_option_name_img_id,
		array(
			'sanitize_callback' => __NAMESPACE__ . '\sanitizeImageID'
		),
	);

	$str_field = uiSettings( 'field_deactivate' );
	$str_label = uiSettings( 'label_checkbox_deactivate' );
	add_settings_field(
		$str_prefix . '_' . $str_field,
		uiSettings( 'title_deactivate' ),
		__NAMESPACE__ . '\cbInputCheckbox',
		$str_option_group,
		$str_prefix . '_section_id',
		array( 'option' => $str_option_name_chk, 'field' => $str_field, 'label' => $str_label ),
	);

	// Is the parent plugin active?
	if ( pluginParentActive() ) {

		$str_title_error = '';

		$arr_post_type_tax_terms = getPostTypeTaxTerms();

		$arr_post_types          = getPostTypes();

		// Field.
		$str_field = 'img_id_global';
		add_settings_field(
			$str_prefix . '_' . $str_field,
			uiSettings( 'label_img_id_global' ),
			__NAMESPACE__ . '\cbInputNumber',
			$str_option_group,
			$str_prefix . '_section_id',
			array(  'class' => 'wpez-ftui-border-top', 'option' => $str_option_name_img_id, 'field' => $str_field, 'label_for' => $str_prefix . '_' . $str_field ),
		);

		foreach( $arr_post_types as $str_post_type => $obj_post_type ) {

			if ( ! ( $obj_post_type instanceof \WP_Post_Type ) ) {
				continue;
			}

			// TODO - remove this conditional as we're checking in getPostTypes().
			if ( true === checkThemeSupport( $str_post_type ) ) {

				$str_field = 'pt_flag_' . $str_post_type;
				$str_label = uiSettings( 'label_checkbox_one' );

				// Post Type opt-in.
				$str_field = 'pt_flag_' . $str_post_type;
				$str_label = uiSettings( 'label_checkbox_one' );
				add_settings_field(
					$str_prefix . '_' . $str_field,
					uiSettings( 'label_img_id_prefix' ) . $obj_post_type->label,
					__NAMESPACE__ . '\cbInputCheckbox',
					$str_option_group,
					$str_prefix . '_section_id',
					array( 'class' => 'wpez-ftui-border-top', 'option' => $str_option_name_chk, 'field' => $str_field, 'label' => $str_label, 'post_type' => $str_post_type ),
				);

				// Post type add img to DB.
				$str_field = 'pt_add_to_db_' . $str_post_type;
				$str_label = uiSettings( 'label_checkbox_two' );
				add_settings_field(
					$str_prefix . '_' . $str_field,
					'',
					__NAMESPACE__ . '\cbInputCheckbox',
					$str_option_group,
					$str_prefix . '_section_id',
					array( 'option' => $str_option_name_chk, 'field' => $str_field, 'label' => $str_label, 'post_type' => $str_post_type ),
				);

				// Post type add img to DB.
				$str_field = 'pt_use_first_img_' . $str_post_type;
				$str_label = uiSettings( 'label_checkbox_use_first_img' );
				add_settings_field(
					$str_prefix . '_' . $str_field,
					'',
					__NAMESPACE__ . '\cbInputCheckbox',
					$str_option_group,
					$str_prefix . '_section_id',
					array( 'option' => $str_option_name_chk, 'field' => $str_field, 'label' => $str_label, 'post_type' => $str_post_type ),
				);

				if ( isset( $arr_post_type_tax_terms[ $str_post_type ] ) && is_array( $arr_post_type_tax_terms[ $str_post_type ] ) && ! empty( $arr_post_type_tax_terms[ $str_post_type ] ) ) {

					if ( 1 === count( $arr_post_type_tax_terms[ $str_post_type ] ) ) {
						// first element from the array.
						$str_tax = key( $arr_post_type_tax_terms[ $str_post_type ] );

						if ( ! empty( $arr_post_type_tax_terms[ $str_post_type ][$str_tax] ) ) {

							// Post type add img to DB.
							$str_field = 'pt_use_tax_' . $str_post_type;
							$str_label = uiSettings( 'label_checkbox_use_tax' );
							add_settings_field(
								$str_prefix . '_' . $str_field,
								'',
								__NAMESPACE__ . '\cbInputCheckbox',
								$str_option_group,
								$str_prefix . '_section_id',
								array( 'option' => $str_option_name_chk, 'field' => $str_field, 'label' => $str_label, 'post_type' => $str_post_type ),
							);
						}

						$arr_hierarchy = array();
						foreach ( $arr_post_type_tax_terms[ $str_post_type ][$str_tax] as $ndx => $obj_term ) {

							$arr_hierarchy[ $obj_term->parent ][ $obj_term->term_id ] = $obj_term;
						}

						displayHierarchy( $arr_hierarchy, 0, '', $str_post_type, $obj_post_type->label, $str_option_group, $str_prefix, $str_option_name_img_id );
					}
				}

				// Post type add img to DB.
				$str_field = 'pt_use_post_' . $str_post_type;
				$str_label = uiSettings( 'label_checkbox_use_post' );
				add_settings_field(
					$str_prefix . '_' . $str_field,
					'',
					__NAMESPACE__ . '\cbInputCheckbox',
					$str_option_group,
					$str_prefix . '_section_id',
					array( 'option' => $str_option_name_chk, 'field' => $str_field, 'label' => $str_label, 'post_type' => $str_post_type ),
				);

				$str_field = 'img_id_' . $str_post_type;
				add_settings_field(
					$str_prefix . '_' . $str_field,
					uiSettings( 'label_img_id_prefix' ) . $obj_post_type->label .  uiSettings( 'label_img_id_suffix' ),
					__NAMESPACE__ . '\cbInputNumber',
					$str_option_group,
					$str_prefix . '_section_id',
					array(  'option' => $str_option_name_img_id, 'field' => $str_field, 'label_for' => $str_prefix . '_' . $str_field ),
				);
			}
		}
	}

	$str_title = uiSettings( 'section_title' ) . $str_title_error;
	// Create settings section.
	add_settings_section(
		$str_prefix . '_section_id',
		$str_title,
		__NAMESPACE__ . '\sectionDescription',
		$str_option_group,
	);
}

/**
 * Displays the terms for the taxonomy of a given post type. TODO - refactor this?
 *
 * @param array $arr_hierarchy
 * @param integer $int_parent
 * @param string $str_level
 * @param string $str_post_type
 * @param string $str_post_type_label
 * @param string $str_option_group
 * @param string $str_prefix
 * @param string $str_option_name_img_id
 *
 * @return void
 */
function displayHierarchy( $arr_hierarchy, $int_parent = 0, $str_level = '', $str_post_type = '', $str_post_type_label = '', $str_option_group = '', $str_prefix = '', $str_option_name_img_id = '' ) {

	$str_level = trim( $str_level );

	$str_level = ( 0 !== $int_parent ) ? $str_level .= '-- ' : '';

	if ( isset( $arr_hierarchy[ $int_parent ] ) ) {

		foreach ( $arr_hierarchy[ $int_parent ] as $int_term_id => $obj_term ) {

			$str_field = 'img_id|' . $str_post_type . '|' . $obj_term->taxonomy . '|'. $obj_term->term_id;
			// Add the field.
			add_settings_field(
				$str_prefix . '_' . $str_field,
				$str_level . uiSettings( 'label_img_id_prefix' ) . $str_post_type_label . ' - ' . $obj_term->taxonomy . ': ' . $obj_term->name . uiSettings( 'label_img_id_suffix' ),
				__NAMESPACE__ . '\cbInputNumber',
				$str_option_group,
				$str_prefix . '_section_id',
				array(  'option' => $str_option_name_img_id, 'field' => $str_field, 'label_for' => $str_prefix . '_' . $str_field ),
			);
			// TODO - If we allow the children to be displayed then we'll need to refactor postTypeTaxTermImgID() as it currently presumes only top level parents.
			$bool_flag = (bool) uiSettings( 'display_tax_term_children_default' );
			if ( apply_filters( __NAMESPACE__ . '\display_tax_term_children', $bool_flag, $str_post_type ) ) {
				displayHierarchy( $arr_hierarchy, $int_term_id, $str_level, $str_post_type, $str_post_type_label, $str_option_group, $str_prefix, $str_option_name_img_id );
			}
		}
	}
	// return;
}


/**
 * Outputs the Settings API section description.
 *
 * @param array $args
 * @return void
 */
function sectionDescription( $args ) {

	$str_error = uiSettings( 'section_title_error' );
	if ( substr( $args['title'], ( -1 * strlen( $str_error ) ) ) === $str_error ) {
		$str_error_msg = uiSettings( 'section_error_msg' );
		echo '<h3>' . wp_kses_post( $str_error_msg ) . '</h3>';
	}
	// TODO - Add this string to the uiSettings().
	echo  '<p>Instructions: See the plugin\'s <a href="https://gitlab.com/WPezPlugins/wpez-force-thumbnail-ui/-/blob/main/README.md">README</a>.';
	echo '<style> tr.wpez-ftui-border-top{border-top: 1px solid rgba(0,0,0,.07)} input.wpez-ftui-num{vertical-align: top} img.wpez-ftui-100-sqr{width: 100px; height: 100px; margin-left: 1rem}</style>';
}

/**
 * Wrapper / helper for WP's standard get_option().
 *
 * @param string $str_option_name
 * @param string $str_field
 * @param mixed $mix_default
 *
 * @return mixed
 */
function getOption( $str_option_name = '', $str_field = '', $mix_default = null ) {

	$arr = get_option( $str_option_name, array() );

	if ( isset( $arr[ $str_field ] ) ) {
		return $arr[ $str_field ];
	}
	return $mix_default;
}

/**
 * Outputs a checkbox.
 *
 * @param array $args
 *
 * @return void
 */
function cbInputCheckbox( $args ) {

	if ( isset( $args['option'], $args['field'], $args['label'] ) ) {

		$str_option = $args['option'];
		$str_field	= $args['field'];
		$str_label  = $args['label'];

		$str_post_type = '';
		if ( isset( $args['post_type'] ) ) {
			$str_post_type = $args['post_type'];
			$str_label     = str_replace( '%post_type%', $str_post_type, $str_label );
		}

		$value = getOption( $str_option, $str_field, '' );
		$value = sanitizeInputCheckboxSingle( $value );

		?>
		<label for="<?php echo esc_attr( $str_option . '_' . $str_field ); ?>">
			<input id="<?php echo esc_attr( $str_option .'_' . $str_field ); ?>" type="checkbox" name="<?php echo esc_attr( $str_option . '[' . $str_field . ']' ); ?>" <?php echo checked( $value ); ?>> <?php echo esc_attr( $str_label ); ?>
		</label>
	<?php
	}
}

/**
 * Sanitizes an array of values from the checkbox inputs.
 *
 * @param array $arr_values
 *
 * @return array
 */
function sanitizeInputCheckbox( array $arr_values ) {

	foreach ( $arr_values as $ndx => $value ) {
		$arr_values[ $ndx ] = sanitizeInputCheckboxSingle( $value );
	}
	return $arr_values;
}

/**
 * Sanitizes a single value from a checkbox input.
 *
 * @param string $value
 *
 * @return bool
 */
function sanitizeInputCheckboxSingle( $value = '' ) {
	// TODO - better way to do this? 
	return ( ! empty( $value ) ) ? true : false;
}


/**
 * Outputs a number input.
 *
 * @param array $args
 *
 * @return void
 */
function cbInputNumber( array $args ) {

	if ( isset( $args['label_for'], $args['option'], $args['field'] ) && ! empty( $args['label_for'] ) ) {

		$str_option     = $args['option'];
		$str_field	    = $args['field'];
		$str_label_for  = $args['label_for'];

		$value = getOption( $str_option, $str_field, 0 );
		$value = sanitizeImageIDSingle( $value );
		?>
		<input id="<?php echo esc_attr( $str_label_for ); ?>" class="wpez-ftui-num" type="number" min="0" name="<?php echo esc_attr( $str_option . '[' . $str_field . ']' ); ?>" value="<?php echo absint( $value ); ?>">
		<?php
		if ( 0 < $value ) {
			$post = get_post( $value );
			if ( $post instanceof \WP_Post ) {

				$arr_img_args = array(
					'class' => 'wpez-ftui-100-sqr',
					'alt'   => $post->post_title,
				);
				$arr_img_args_new = apply_filters( __NAMESPACE__ . '\admin_settings_img_args', $arr_img_args, $value, $post, $args );
				if ( is_array( $arr_img_args_new ) ) {
					$arr_img_args = $arr_img_args_new;
				}
				$str_img = wp_get_attachment_image( $value, 'thumbnail', false, $arr_img_args );
				if ( ! empty( $str_img ) ) {
					echo $str_img;
				}
			}
		}
	}
}

/**
 * Sanitizes an array of values from the number inputs
 *
 * @param array $arr_values
 *
 * @return array
 */
function sanitizeImageID( array $arr_values ) {

	foreach ( $arr_values as $ndx => $value ) {
		$arr_values[ $ndx ] = sanitizeImageIDSingle( $value );
	}
	return $arr_values;
}
/**
 * Sanitizes a single value from a number input.
 *
 * @param integer $value Post ID of an image.
 *
 * @return integer
 */
function sanitizeImageIDSingle( $value = 0 ) {

	$value = absint( $value );
	if ( 'attachment' === get_post_type( $value ) ) {
		return $value;
	}
	// If the ID isn't for an attachment, return 0.
	return 0;
}

/**
 * Gets all taxonomies and then maps those + their terms to post types the taxonomy is registered to. Returned: $arr[ $str_post_type ][ $str_tax ] = $arr_terms.
 *
 * @return array Looks like: $arr[ $str_post_type ][ $str_tax ] = $arr_terms
 */
function getPostTypeTaxTerms() {

	static $arr_post_type_tax_terms;

	if ( null === $arr_post_type_tax_terms ) {

		$arr_taxs_all = get_taxonomies( array(), 'objects' );

		$arr_post_type_tax_terms = array();
		foreach ( $arr_taxs_all as $str_tax => $obj_tax ) {

			// we only want public taxonomies that are hierarchical. TODO - also non-hierarchical??
			if ( ! $obj_tax instanceof \WP_Taxonomy || ! $obj_tax->public || ! $obj_tax->hierarchical ) {
				continue;
			}

			// get all terms for this taxonomy
			$arr_terms = get_terms( array( 'taxonomy' => $str_tax, 'hide_empty' => false ) );
			// map the terms to the post_type + taxonomy.
			foreach ( $obj_tax->object_type as $str_post_type ) {
				$arr_post_type_tax_terms[ $str_post_type ][ $str_tax ] = $arr_terms;
			}
		}
	}
	return $arr_post_type_tax_terms;
}

/**
 * Take an array of WP tax terms (from get_terms()): (1) "flattens" the array so that each term_id key is paired with its "top level" parent (i.e., term with parent 0). (2) Also returns an array of depth[ int ] => array( term_id(s)... ) for that depth.
 *
 * @param array $arr_orig The array of terms from get_terms() or similar.
 * @param array $arr_new Terms' term_ids are shifted into this array as their top level parent is identified.
 * @param array $arr_depth Terms' terms_ids are stored in this array at the depth they are found. Depth 0 is the top level (parents).
 * @param int   $int_depth The current depth of the term being processed. Starts at 0 and increments "down" the tree.
 * 
 * @return array  Associative array of two different arrays; 'flat' and 'depth'. 'flat' is an array of term_id => top level parent_id. 'depth' is an array of int_depth => array( term_id(s) at that depth).
 */
function getTermsFlattened( array $arr_orig, array $arr_new = array(), array $arr_depth = array(), int $int_depth = 0 ) {

	if ( empty( $arr_new ) ) {
		foreach ( $arr_orig as $key => $obj_term ) {

			if ( ! ( $obj_term instanceof \WP_Term ) ) {
				continue;
			}
			// if it's a parent, add it and assign the key as the value (so it kinda points to itself). We want a term_id to top parent map.
			if ( 0 === $obj_term->parent ) {
				$arr_new[ $obj_term->term_id ] = $obj_term->term_id;
				unset( $arr_orig[ $key ] );
				$arr_depth[ $int_depth ][] = $obj_term->term_id;
			}
		}
	}
	$int_depth++;
	// Shift the originals into the new and assign the value (which is the top parent ).
	foreach ( $arr_orig as $key => $obj_term ) {

		if ( isset( $arr_new[ $obj_term->parent ] ) ) {
			$arr_new[ $obj_term->term_id ] = $arr_new[ $obj_term->parent ];
			unset( $arr_orig[ $key ] );
			$arr_depth[ $int_depth ][] = $obj_term->term_id;
		}
	}

	// If we're moved all the originals then we're done.
	if ( empty( $arr_orig ) ) {
		return array(
			'flat'  => $arr_new,
			'depth' => $arr_depth,
		);
	}
	// else, recurse.
	return getTermsFlattened( $arr_orig, $arr_new, $arr_depth, $int_depth );
}

/**
 * Gets the public post types that have theme support for 'post-thumbnails'.
 *
 * @return array
 */
function getPostTypes() {

	$args = array(
		'public' => true,
	);

	$arr = get_post_types( $args, 'objects' );

	// remove the attachment post type.
	unset( $arr['attachment'] );

	foreach ( $arr as $str_post_type => $obj_post_type ) {
		if ( ! $obj_post_type instanceof \WP_Post_Type || true !== checkThemeSupport( $str_post_type ) ) {
			unset( $arr[ $str_post_type ] );
		}
	}

	return $arr;
}

add_filter( __NAMESPACE__ . '\use_post_type_tax_flag', __NAMESPACE__ . '\usePostTypeTaxFlag', 10, 3 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\use_post_type_tax_flag'.
 *
 * @param bool $bool
 * @param integer $int_thumb_id
 * @param \WP_Post $post
 *
 * @return bool
 */
function usePostTypeTaxFlag( $bool, int $int_thumb_id, \WP_Post $post ) {

	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_checkbox_suffix' );
	$value   = getOption( $str_opt, 'pt_use_tax_' . $post->post_type, '' );
	return sanitizeInputCheckboxSingle( $value );
}


add_filter( __NAMESPACE__ . '\post_type_tax_term_img_id', __NAMESPACE__ . '\postTypeTaxTermImgID', 10, 2 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\post_type_tax_term_img_id'.
 *
 * @param integer $int
 * @param \WP_Post $post
 *
 * @return integer
 */
function postTypeTaxTermImgID( int $int_img_id, \WP_Post $post ) {

	// Is the checkbox checked?
	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_checkbox_suffix' );
	$value   = getOption( $str_opt, 'pt_use_tax_' . $post->post_type, '' );
	$value   = sanitizeInputCheckboxSingle( $value );

	if ( ! $value ) {
		return 0;
	}

	// Gets the mapped array from the UI settings.
	$arr_ui_pt_tax_term_img_ids = getUIPostTypeTaxTermImgIDs();

	// Let see where we stand.
	if ( ! isset( $arr_ui_pt_tax_term_img_ids[ $post->post_type ] ) || ! is_array( $arr_ui_pt_tax_term_img_ids[ $post->post_type ] ) || empty( $arr_ui_pt_tax_term_img_ids[ $post->post_type ] ) ) {
		return $int_img_id;
	}

	// What is the tax for this post type? key() takes the first element from the array.
	// TODO - refactor the UI to pick the taxonomy when the post type has more than one that's public and hierarchical.
	$str_post_type_tax = key( $arr_ui_pt_tax_term_img_ids[ $post->post_type ] );
	// Get the terms for this post for this tax.
	$arr_post_terms = get_the_terms( $post, $str_post_type_tax );

	// Do we have some terms to work with?
	if ( ! is_array( $arr_post_terms ) || empty( $arr_post_terms ) ) {
		return $int_img_id;
	}

	// Let's see if we have any terms that are "top level" parents.
	$arr_post_terms_new = array_filter(
		$arr_post_terms,
		function ( $var ) {
			return 0 === $var->parent;
		}
	);

	// If we have top level parents, lets see if one of them has an UI img_id !== 0 for the term_id of the parent.
	if ( ! empty( $arr_post_terms_new ) ) {
		foreach ( $arr_post_terms_new as $obj_term ) {
			if ( isset( $arr_ui_pt_tax_term_img_ids[ $post->post_type ][ $str_post_type_tax ][ $obj_term->term_id ] ) && 0 < $arr_ui_pt_tax_term_img_ids[ $post->post_type ][ $str_post_type_tax ][ $obj_term->term_id ] ) {
				$int_img_id = $arr_ui_pt_tax_term_img_ids[ $post->post_type ][ $str_post_type_tax ][ $obj_term->term_id ];
				return $int_img_id;
			}
		}
	}

	// Since we didn't find a top level parent with an img_id !== 0  let's look at the remaining terms that are "children". That is, remove the (useless) parents and keep looking (at the children). 
	$arr_diff = array_diff_key( $arr_post_terms, $arr_post_terms_new );

	// rework the array of (leftover) terms so that the keys are the term_id.
	$arr_post_terms_new = array();
	foreach ( $arr_diff as $key => $obj_term ) {
		$arr_post_terms_new[ $obj_term->term_id ] = $obj_term;
	}

	// Get_the array: [ 'post_type'][ 'taxonomy' ] = array tax_terms.
	$arr_pt_tax_terms = getPostTypeTaxTerms();

	if ( ! isset( $arr_pt_tax_terms[ $post->post_type ][ $str_post_type_tax ] ) || ! is_array( $arr_pt_tax_terms[ $post->post_type ][ $str_post_type_tax ] ) ) {
		return $int_img_id;
	}

	// Work the flattened magic.
	$arr_flattened = getTermsFlattened( $arr_pt_tax_terms[ $post->post_type ][ $str_post_type_tax ] );

	// Make sure we get something back that we can use.
	if ( ! isset( $arr_flattened['flat'], $arr_flattened['depth'] ) || ! is_array( $arr_flattened['flat'] ) || ! is_array( $arr_flattened['depth'] ) ) {
		return $int_img_id;
	}

	// Reverse array (but keep the keys), because we want to start at "the bottom" and work our way up. That is, lower level terms have a high priority because they are (in theory) more specific.
	$arr_flattened['depth'] = array_reverse( $arr_flattened['depth'], true );

	// We'll start at the bottom depth and work our way up.
	foreach ( $arr_flattened['depth'] as $int_depth => $arr_term_ids ) {
		if ( ! is_array( $arr_term_ids ) || empty( $arr_term_ids ) ) {
			continue;
		}
		foreach ( $arr_term_ids as $int_term_id ) {
			// child term id...get the top level parent term_id from ['flat']...do we have an 0 !== img_id for this term_id?
			if ( isset( $arr_post_terms_new[ $int_term_id ] ) && isset( $arr_flattened['flat'][ $int_term_id ] )
			&& ( isset( $arr_ui_pt_tax_term_img_ids[ $post->post_type ][ $str_post_type_tax ][ $arr_flattened['flat'][ $int_term_id ] ] )
			&& 0 < $arr_ui_pt_tax_term_img_ids[ $post->post_type ][ $str_post_type_tax ][ $arr_flattened['flat'][ $int_term_id ] ] ) ) {
				// we have a winner!
				$int_img_id = $arr_ui_pt_tax_term_img_ids[ $post->post_type ][ $str_post_type_tax ][ $arr_flattened['flat'][ $int_term_id ] ];
				return $int_img_id;
			}
		}
	}
	return $int_img_id;
}

/**
 * Using the data from the UI settings, returns an array: [post_type][taxonomy][term_id] = img_id.
 *
 * @return array
 */
function getUIPostTypeTaxTermImgIDs() {

	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_img_id_suffix' );
	$arr_ids = get_option( $str_opt, array() );

	// We only want to calculate this once.
	static $arr_return;

	if ( null === $arr_return ) {
		$arr_return = array();
		foreach ( $arr_ids as $str_slug => $img_id ) {
			// count the number of | in the $str_slug - we're looking for: 'img_id|' post type | taxonomy | term_id
			if ( $img_id < 1 || 0 !== strpos( $str_slug, 'img_id|' ) || 3 !== substr_count( $str_slug, '|' ) ) {
				continue;
			}

			$arr_exp = explode( '|', $str_slug );
			// 0 = 'img_id' (which don't use here), 1 = post type, 2 = taxonomy, 3 = term_id => img_id .
			$arr_return[ $arr_exp[1] ][ $arr_exp[2] ][ $arr_exp[3] ] = $img_id;
		}
	}
	return $arr_return;
}


add_filter( __NAMESPACE__ . '\use_first_content_img_flag', __NAMESPACE__ . '\useFirstContentImgFlag', 10, 3 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\use_first_content_img_flag'.
 *
 * @param bool $bool
 * @param integer $int_thumb_id
 * @param \WP_Post $post
 *
 * @return bool
 */
function useFirstContentImgFlag( $bool, int $int_thumb_id, \WP_Post $post ) {
	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_checkbox_suffix' );
	$value   = getOption( $str_opt, 'pt_use_first_img_' . $post->post_type, '' );
	return sanitizeInputCheckboxSingle( $value );
}


// Filter in the parent plugin.
add_filter( __NAMESPACE__ . '\post_type_blacklist', __NAMESPACE__ . '\postTypeBlacklist', 10, 1 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\post_type_blacklist'.
 *
 * @param array $arr
 *
 * @return array
 */
function postTypeBlacklist( array $arr ) {

	$arr_pt     = getPostTypes();
	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_checkbox_suffix' );

	// We only want to calculate this once.
	static $arr_new;

	if ( null === $arr_new ) {
		$arr_new = array();
		foreach ( $arr_pt as $str_post_type => $obj_post_type ) {

			$value = getOption( $str_opt, 'pt_flag_' . $str_post_type, '' );
			// if the pt_flag checkbox isn't checked then we'll consider that "blacklisted".
			if ( empty( $value ) ) {
				$arr_new[] = $str_post_type;
			}
		}
	}
	return array_merge( $arr, $arr_new );
}

add_filter( __NAMESPACE__ . '\last_chance_img_id', __NAMESPACE__ . '\lastChanceImgID', 10, 1 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\last_chance_img_id'.
 *
 * @param integer $int
 *
 * @return integer
 */
function lastChanceImgID( $int ) {

	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_img_id_suffix' );
	$value   = getOption( $str_opt, 'img_id_global', '' );
	return sanitizeImageIDSingle( $value );
}


add_filter( __NAMESPACE__ . '\set_post_thumbnail_flag', __NAMESPACE__ . '\setPostThumbnailFlag', 10, 3 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\set_post_thumbnail_flag'.
 *
 * @param bool $bool
 * @param integer $int_thumb_id
 * @param \WP_Post $post
 * 
 * @return bool
 */
function setPostThumbnailFlag( $bool, $int_thumb_id, \WP_Post $post ) {

	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_checkbox_suffix' );
	$value   = getOption( $str_opt, 'pt_add_to_db_' . $post->post_type, '' );

	return (bool) $value;
}

add_filter( __NAMESPACE__ . '\post_type_img_id', __NAMESPACE__ . '\postTypeImgID', 10, 2 );
/**
 * Callback for the parent plugin filter: __NAMESPACE__ . '\post_type_img_id'.
 *
 * @param integer $int
 * @param \WP_Post $post
 *
 * @return integer
 */
function postTypeImgID( $int, \WP_Post $post ) {

	// Is the checkbox checked?
	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_checkbox_suffix' );
	$value   = getOption( $str_opt, 'pt_use_post_' . $post->post_type, '' );
	$value   = sanitizeInputCheckboxSingle( $value );

	if ( ! $value ) {
		return 0;
	}

	// Get the img ID for the post_type
	$str_opt = uiSettings( 'option_namespace' ) . uiSettings( 'option_img_id_suffix' );
	$value   = getOption( $str_opt, 'img_id_' . $post->post_type, '' );
	return sanitizeImageIDSingle( $value );
}

