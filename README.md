# Plugin Name: WPezPlugins: Force Thumbnail UI
### A WordPress Plugin

Description: Admin UI for WPezPlugins: Force Thumbnail (https://gitlab.com/WPezPlugins/wpez-force-thumbnail).

If you don't want to muck with the filters in the parent plugin then this plugin provides a UI via the WordPress Settings API.

Under the hood the plugin using the following hierarchy / logic to find an image when there is no Featured Image / Post Thumbnail (FI / PT) assigned to a given Post Type:
- If possible, the First Image from the_content.
- If possible, use an image (ID) defined for a Taxonomy -> Term (e.g., Category -> Some Category).
- Use the image defined for the Post Type
- And when the above steps don't yield an image (ID), use the Global Img ID.

Notice how the hierarchy starts are close to the single post as possible (i.e., image from the content) and then step by step gets further way, until the last chance of the Global Img ID. 

**Important:** First Image, Taxonomy-> Term match, and Post Type are each optional. That is, they each have a checkbox for opt'ing in / out of that step in the hierarchy.

#### -- IMPORTANT --

If you can't test this plugin locally or in a staging environment, then please backup your site before trying this plugin. It's possible, given the setting below, you might make changes to your DB that can't be undone without a backup.
#### ----

#### Explanation of Settings

- **Checkbox: "Remove all Force Thumbnail data from the DB when the plugin is deactivated."**  - If you want to remove this plugin and all it's associated data then check this prior to deactivating (and then deleting). If you want to temporarily deactivate this plugin and not lose the settings, then let this unchecked.
.
- **Input number: Global Img ID** - The (post) ID from an image in the WP Media Library. Per the hierarchy above, this is the "last chance" image.
.
- **Checkbox: "Enable Force Thumbnail for Post Type: _____"** - Check if you want the Force Thumbnail plugin to try to find a FI / PT for the Post Type displayed with the checkbox. Otherwise (i.e., unchecked), the plugin will ignore this Post Type completely. That is, any subsequent settings for the Post Type will be ignored. This is a simple on / off switch at the post type level.
.
- **Checkbox: "Add the Force Thumbnail image to the database for Post Type: ____"** - Check if you want the image the Force Thumbnail plugin finds to be physically added to the database for the post / Post Type being evaluated. On the other hand, if left unchecked the plugin will apply its logic every time the WP filter 'post_thumbnail_id' is called and return a value (so an images get displayed). On low traffic sites unchecked is likely fine. But for heavier traffic sites, once you're happy with the results you're getting from the other settings, it's recommended you check this box to reduce the load of having to re-run the same logic over and over and over. **Important:** Once the DB is updated it can't be undone (via this plugin). You'd have to restore a DB backup. 
.
- **Checkbox: "Use the first image from The Content for the Post Type: ____"** - Check if you want the plugin to try to get the FI / PT from the post's content. If this is not checked, looking in the content will be bypassed.
.
- **Use the Taxonomy (list below) for the Post Type: ____"** - Check if you want to try to match the post's Taxonomy -> Terms to find the FI / PT.  If this is not checked, matching Taxonomy -> Terms will be bypassed. **Important:** Currently, there's no "special considerations" built into the matching. Just a loop over the Post's Tax's Terms that stops once it find a match in the array from the UI Settings. That match could be a parent Term. It could be a child. For this reason, you'll get the most consistent visual results if your Parent Term and all its Child Terms use the same image (ID). 
.
- **Input number: Post Type: _____ - (Taxonomy) _____: (Term) _____ > Img ID** - The (post) ID from an image in the Media Library for this Post Type + Taxonomy + Term.
.
- **Checkbox: "Use the Img ID (in the field below) for the Post Type: ____"** - Check if you want the plugin to - if necessary - use the image ID (in the input field below it) for the FI / PT for this post type. If not checked this step in the hierarchy will be ignored (even if you've added an Img ID below it).
.
- - **Input number: Post Type: _____ > Img ID** - The (post) ID from an image in the Media Library for this Post Type.
.
#### Bugs, Questions, Issues, Etc.

Please uses the Issues page for the plugin on GitLab.

https://gitlab.com/WPezPlugins/wpez-force-thumbnail-ui/-/issues













